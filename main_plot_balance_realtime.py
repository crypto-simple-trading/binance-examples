import sys
import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from binance_requests import BinanceClient
from utils import doPlot, processOneCoin

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--apiFilePath", required=True)
    parser.add_argument("--mode", required=True)
    parser.add_argument("--savePath")
    parser.add_argument("--minBusdValue", type=float, default=10)
    parser.add_argument("--maxNumValues", type=int, default=1<<31)
    args = parser.parse_args()
    assert args.mode in ("save", "show")
    if args.mode == "save":
        assert not args.savePath is None
    return args

def balanceGenerator(client:BinanceClient, minBusdValue:float, maxNumValues:int):
    coinsStats = {}
    while True:
        try:
            data = client.getAccountBalance()
        except Exception as e:
            print(str(e))
            continue

        coins = data["balance"]
        validThisIter = []
        for coin in coins:
            if not coin in coinsStats:
                coinsStats[coin] = []

            busdValue = coins[coin][2]
            if busdValue < minBusdValue:
                continue
            coinsStats[coin] = processOneCoin(coinsStats[coin], busdValue, maxNumValues)
            validThisIter.append(coin)
        yield {k : coinsStats[k] for k in validThisIter}

def main():
    args = getArgs()
    client = BinanceClient(args.apiFilePath)
    g = balanceGenerator(client, args.minBusdValue, args.maxNumValues)

    while True:
        try:
            res = next(g)
        except Exception as e:
            print(str(e))
            breakpoint()
            continue
        doPlot(res)
        total = sum([res[k][-1] for k in res])
        plt.suptitle("Total: %2.5f BUSD" % total)

        if args.mode == "show":
            plt.pause(0.05)
        else:
            plt.savefig(args.savePath)

if __name__ == "__main__":
    main()