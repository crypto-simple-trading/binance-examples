import numpy as np
import matplotlib.pyplot as plt
from typing import List

def plotOneCoin(ax, values, title):
        Mean = np.mean(values)
        ax.clear()
        ax.plot(np.arange(len(values)), values, color="blue")
        ax.plot(np.arange(len(values)), np.repeat(Mean, len(values)), color="red")
        conv = np.convolve(values, np.ones(6) / 6, mode="full")[0 : len(values)]
        print(len(conv))
        ax.plot(np.arange(len(values)), conv, color="green")
        ax.set_title(title)

def doPlot(res):
    plt.close()
    N = np.sqrt(len(res))
    N = len(res) if N == int(N) else (int(N) + 1)**2
    N = max(2, int(np.sqrt(N)))
    fig, ax = plt.subplots(N, N, figsize=(4 * N, 3 * N))
    total = 0
    for i, coin in enumerate(res):
        arr = res[coin]
        plotOneCoin(ax=ax[i // N, i % N], values=arr, title=coin)
        total += arr[-1]

def processOneCoin(historyData:List[float], newValue:float, maxNumValues:int=1<<31) -> List[float]:
    Mean = np.mean(historyData) if len(historyData) > 0 else 0
    Rap = newValue / (Mean + np.spacing(1))
    # If big change, reset the array (we just sold/bought this coin)
    if Rap > 1.05 or Rap < 0.95:
        historyData = []
    historyData.append(newValue)
    if len(historyData) > maxNumValues:
        historyData = historyData[-maxNumValues : ]
    return historyData
