import time
from argparse import ArgumentParser
from binance_requests import BinanceClient

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--apiFilePath", required=True)
    parser.add_argument("--symbol", required=True)
    parser.add_argument("--side", required=True)
    parser.add_argument("--price", type=float, required=True)
    parser.add_argument("--quantity", type=float, required=True)
    args = parser.parse_args()
    return args

def main():
    args = getArgs()
    client = BinanceClient(secretFilePath=args.apiFilePath)
    order = client.openNewOrder(args.symbol, args.side, args.price, args.quantity)
    print("New order opened with id: %s" % order["orderId"])


if __name__ == "__main__":
    main()