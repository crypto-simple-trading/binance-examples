# Binance Requests examples

## __Description__:
Repository with various examples using [Binance Requests library](https://www.gitlab.com/binance-simple-trading/binance-requests)

## __Examples__:

1. Price tracker [main_plot_tracker_realtime.py]()
 - Plots the price changes of various coins (defined by --coins=...). Exports to png file (to be used by web server, for example) or plots using matplotlib in realtime updating fashion. Would be nice to be able to use it as API or export to some file/database.

2. Balance tracker [main_plot_balance_realtime.py]()
 - Similar to price tracker, but plots the changes of balance for a specific account (as defined by the api file) for all coins (above 10BUSD value)

3. Open orders [main_open_orders.py]()
 - Prints all open orders of a specific account (as defined by the api file)